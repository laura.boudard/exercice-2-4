### Bienvenue dans ce cours CNAM en FOAD, NF035

Vous trouverez dans ce fichier **Readme** l'énoncé de chaque exercice noté dans le cadre du projet tutoré central à cet enseignement sur **JAVA: Bibliothèques et Patterns**.
_La solution de chaque exercice fera d'ailleurs l'objet d'un nouveau projet Git ici-même._

**Même si votre solution est bonne et recevable avec tous les points, il est par ailleurs préférable de repartir de la présente solution pour avancer dans ce projet tutoré sans risque, étant donné ce qui vous sera demandé en suite.**

Voici l'énoncé de l'exercice qui a fait l'objet de la présente solution, avec en introduction, un bref rappel des objectifs de la session d'exercices.
Il s'agit du 3ème exercice noté de la 2ème session (=>Bloc d'enseignement) de ce projet.

---

# Listes, Ensembles et Couches logicielles
 ++ Intégration d'une couche logicielle transverse: commons/utils

## Contexte
* Au programme de ce cours: Listes, Ensembles, Couches logicielles
## Objectifs
* Mises en application:
 - [x] (Exercice 1) Homogénéisation du projet en suivant le standard Maven (Exercice 1)
 - [x] Permettre au wallet de recevoir plusieurs badges, et de les restituer sous forme de liste
    - [x] (Exercice 2) Ecriture/Lecture séquentielle mono-ligne dans le fichier texte, avec métadonnées simples, ID, Taille.
    - [x] (Exercice 3) Lecture Du fichier texte en accès direct: stockage des métadonnées trouvées dans une Liste + Lecture d'un badge correspondant aux méta-données
 - [x] **(Exercice 4) Plus de méta-données pour permettre au wallet de recevoir plusieurs badges, et de les restituer sous forme de Set**
    - [ ] Lecture Du fichier texte en accès direct et stockage des métadonnées trouvées dans un Set, donc... 
    - [ ] Ajout d'une fonction de hachage dans les métadonnées... hummm ... changement de plan, ajout de champs fonctionnels relatifs aux badges et définissant leur unicité :) 
    - [ ] Ajout d'une gestion d'exception lors de l'ajout de badge pour garantir l'unicité des badges.


## Consignes de l'Exercice 3

Abordons à présent l'exercice 4 de cette Séance, avec comme défi cette fois de garantir l'unicité d'un badge
Pour cela nous allons d'abord ajouter un certain nombre de données supplémentaires à notre objet DigitalBadge, non pas de métadonnées relative à l'image du badge en tant que telles, mais des informations qui, s'y ajoutant, oeuvent donner valeur probante à celui-ci, comme par exemple une date d'obtention ou encore un code de série unique.

 - [ ] Ajouter donc à notre Objet les attributs (champs, getters, setters) indiqués dans ce diagramme:
```plantuml
@startuml
title __MODEL's Class Diagram__\n

  namespace fr.cnam.foad.nfa035.badges.wallet.model {
    class fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge {
        - badge : File
        - begin : Date
        - end : Date
        - serial : String
        + DigitalBadge()
        + equals()
        + getBadge()
        + getBegin()
        + getEnd()
        + getMetadata()
        + getSerial()
        + hashCode()
        + setBadge()
        + setBegin()
        + setEnd()
        + setMetadata()
        + setSerial()
        + toString()
    }
  }
  

  namespace fr.cnam.foad.nfa035.badges.wallet.model {
    class fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata {
        - badgeId : int
        - imageSize : long
        - walletPosition : long
        + DigitalBadgeMetadata()
        + equals()
        + getBadgeId()
        + getImageSize()
        + getWalletPosition()
        + hashCode()
        + setBadgeId()
        + setImageSize()
        + setWalletPosition()
        + toString()
    }
  }
 
  fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge o-right- fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata : metadata

@enduml
```
 - [ ] Ensuite, choisissez judicieusement les champs à intégrer dans les 2 méthodes **equals()**, **hashcode()** (et éventuellement **toString()**). A priori, le code série associé à la date de péremption sont le minimum, et qui peu le plus peu le moins ;)
 - [ ] Changez la signature du champ **List<DigitaBadgeMetadata> metas** vers **Set<DigitaBadgeMetadata> metas** dans la classe **WalletDeserializerDirectAccessImpl**, puis répercutez ce changement dans tout le code pour que le projet compile.
 - [ ] Exécutez vos tests Unitaires, que constatez-vous ?
    - [ ] Il y a un impact sur un test, prenez cette version qui compile:
```java
    @Test
    void testGetMetadata() throws IOException {
        DirectAccessBadgeWalletDAO dao = new DirectAccessBadgeWalletDAOImpl(RESOURCES_PATH + "db_wallet_indexed.csv");
        Set<DigitalBadgeMetadata> metaSet = dao.getWalletMetadata();
        LOG.info("Et voici les Métadonnées: {}", dao.getWalletMetadata());

        assertEquals(3, metaSet.size());
        Iterator it = metaSet.iterator();
        assertEquals(new DigitalBadgeMetadata(1, 0,557), it.next());
        assertEquals(new DigitalBadgeMetadata(2,753, 906), it.next());
        assertEquals(new DigitalBadgeMetadata(3,1972, 35664), it.next());
    }
```
    - [ ] Le test fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAOTest#testGetMetadata est devenu rouge (ou en échec)
    - [ ] Interessez vous à la documentation du Set et de comment veiller à ce que l'ordre n'aturel de tri dans le set corresponde toujours à l'ordre d'insertion
```plantuml
@startuml

title __MODEL's Class Diagram__\n

  namespace fr.cnam.foad.nfa035.badges.wallet.model {
    class fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge {
        - badge : File
        - begin : Date
        - end : Date
        - serial : String
        + DigitalBadge()
        + DigitalBadge()
        + compareTo()
        + equals()
        + getBadge()
        + getBegin()
        + getEnd()
        + getMetadata()
        + getSerial()
        + hashCode()
        + setBadge()
        + setBegin()
        + setEnd()
        + setMetadata()
        + setSerial()
        + toString()
    }
  }
  

  namespace fr.cnam.foad.nfa035.badges.wallet.model {
    class fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata {
        - badgeId : int
        - imageSize : long
        - walletPosition : long
        + DigitalBadgeMetadata()
        + DigitalBadgeMetadata()
        + compareTo()
        + equals()
        + getBadgeId()
        + getImageSize()
        + getWalletPosition()
        + hashCode()
        + setBadgeId()
        + setImageSize()
        + setWalletPosition()
        + toString()
    }
  }
  

  fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge .up.|> java.lang.Comparable
  fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge o-right- fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata : metadata
  fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata .up.|> java.lang.Comparable
@enduml
```
 - [ ] Enfin à l'aide du diagramme ci-dessus, du Modèle à jour, appliquer la logique permettant de garantir un tri par ordre d'insertion initial...mais aidez-vous aussi de cette modification dans le test :
```java
Iterator it = new TreeSet(metaSet).iterator();
```
   - [ ] Astuce pour les comparables, la méthode **compareTo** de l'objet agrégat **DigitalBadge** peut simplement appeler la methode **compareTo** de son objet agrégé **DigitalBadgeMetadata**, car c'est à son niveau que se situe le numéro **idBadge** correspondant au placement dans le fichier csv ;) 
 - [ ] Donc ... on est bien content, on a un Set<> en retour, nos badges sont Uniques, mais teste-t-on vraiment l'impossibilité d'insérer 2x le même badge. Mais c'est vrai d'ailleurs que l'on a pas géré la sérialisation/désérialisation des nouveaux champs...donc:
 - [ ] Il va falloir changer pas mal de choses pour intégrer ce nouveau format:
   - [ ] dans l'interface **DirectAccessDatabaseDeserializer**, au niveau de la methode **deserialize()** déclarer un objet **DigitalBadge** et non **DigitalBadgeMetadata**
     - [ ] impacter le reste du code en implémentant ce qu'il faut pour que cela compile de nouveau
   - [ ] Modifier la signature de la classe **WalletSerializerDirectAccessImpl** comme ci-dessous
     - [ ] et aussi bien sûr impacter le reste du code en implémentant ce qu'il faut pour que cela compile de nouveau
   ```java
   public class WalletSerializerDirectAccessImpl
        extends AbstractStreamingImageSerializer<DigitalBadge, WalletFrameMedia>
   ```
   - [ ] Penser autrement la métadonnée, et accepter de retourner des Set<DigitalBadge> en tant que tel, car tout est métadonnée finalement ;)
     - [ ] Donc pour ce faire, prenez cette nouvelle version de la classe de test [**DirectAccessBadgeWalletDAOTest**](help/badges-wallet/src/test/java/fr/cnam/foad/nfa035/badges/wallet/dao/DirectAccessBadgeWalletDAOTest.java)
     - [ ] Et aussi cette nouvelle version de [**DirectAccessBadgeWalletDAOImpl**](help/badges-wallet/src/main/java/fr/cnam/foad/nfa035/badges/wallet/dao/impl/DirectAccessBadgeWalletDAOImpl.java) (son interface est logiquement à impacter aussi...)
     - [ ] Et encore enfin, cette nouvelle version de la méthode **deserialize** de la classe **MetadataDeserializerDatabaseImpl** ;) ... et puis pour le reste à vous de jouer !!
```java
@Override
public Set<DigitalBadge> deserialize(WalletFrameMedia media) throws IOException {
    DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
    BufferedReader br = media.getEncodedImageReader(false);
    return br.lines()
            .map(
                l-> l.split(";")
            )
            .filter(s -> s.length == 7)
            .map(
                    s -> {
                        Date begin=null,end=null;
                        try {
                            begin = format.parse(s[4]);
                        } catch (ParseException e) {
                            LOG.error("Problème de parsage, on considère la date Nulle", e);
                        }
                        try {
                            end = format.parse(s[5]);
                        } catch (ParseException e) {
                            LOG.error("Problème de parsage, on considère la date Nulle", e);
                        }
                        return new DigitalBadge(s[3],begin,end,new DigitalBadgeMetadata(Integer.parseInt(s[0]),Long.parseLong(s[1]),Long.parseLong(s[2])),null);
                    }
            )
            .collect(Collectors.toSet());
}
```    
 - [ ] Last but not least... **n'oubliez pas votre preuve en image!**
   
  ![preuve][preuve]

[preuve]: screenshots/All-Tests-2.png "Echantillon principal"


----

### RESSOURCES

 * Pour vous aider, (il est possible d'avoir des problèmes d'encodage donc faisons un geste...), et malgré le fait que vous devriez penser à sa mise à jour, voici le csv échantillon pour la nouvelle version du test Unitaire, à placer dans les ressources de test:
  * [db_wallet_indexed.csv](badges-wallet/src/test/resources/db_wallet_indexed.csv), à télécharger au format brut.


